# function HelloWorld {
#     return "Hello World!"
# }

# function OddOrEven ($num) {
#     $ret = If ($num % 2 -eq 0) {"Even"} Else {"Odd"}
#     return $ret
# }

# function GreaterNum($num1, $num2) {
#     if ($num1 -gt $num2){
#         return $num1
#     }
#     return $num2
# }

# HelloWorld

# Write-Output "4 is ${OddOrEven 4}"

# GreaterNum 45 54

# function TestPipeline {
#     begin {Write-Output "Beginning"}
#     process {Write-Output "Processing $_"}
#     end{Write-Output "Ending"}
# }

# @(1..20) | TestPipeline

$a = 800
while ($a -gt 300) {
    $keyobj = $host.ui.RawUI.ReadKey()
    $char = $keyobj.Character
    switch ($char) {
        'w' {Write-Output "Move Forward"}
    }
    $a -= 1
    start-sleep -Millisecond 1
    Write-Output $a
}