<#
Class for cards. Cards are represented by value and suit. 
Value is represented as an int from 1-13 and converted to 
it's string representation in the "ToString()" function
#>
class Card {
    [int]$value
    [string]$suit

    static $valueToStringMap = @{1="Ace";2="Two";3="Three";4="Four";5="Five";
                                6="Six"; 7="Seven"; 8="Eight";9="Nine"; 10="Ten"; 
                                11="Jack"; 12="Queen";13="King"}

    Card([int]$value, [string]$suit) {
        $this.value = $value
        $this.suit = $suit
    }

    [string] ToString() {
        return "$([Card]::valueToStringMap[$this.value]) of $($this.suit)"
    }
}

$test = 1, 2, 3, 4, 5





