<#
Class for cards. Cards are represented by value and suit. 
Value is represented as an int from 1-13 and converted to 
it's string representation in the "ToString()" function
#>
class Card {
    [int]$value
    [string]$suit

    static $valueToStringMap = @{1="Ace";2="Two";3="Three";4="Four";5="Five";
                                6="Six"; 7="Seven"; 8="Eight";9="Nine"; 10="Ten"; 
                                11="Jack"; 12="Queen";13="King"}

    Card([int]$value, [string]$suit) {
        $this.value = $value
        $this.suit = $suit
    }

    [string] ToString() {
        return "$([Card]::valueToStringMap[$this.value]) of $($this.suit)"
    }
}

<#
Checks for runs in the currently played cards and scores 
them appropriately. This function applies to phase 2, or
the "pegging" phase
#>
function scoreRunsPhase2($cards) {
    while ($cards.Length -gt 0) {
        # Get unique values from cards being evaluated
        $valueSet = $cards | Select-Object -Property value -Unique

        # Measure the count, max, and min values in the set
        $measures = $valueSet | Measure-Object -Property value -Maximum -Minimum
        
        # If there is potential for a run (set length > 2), continue checking 
        if ($measures.Count -gt 2) {
            # Range will be the difference between the highest and lowest value cards
            $range = $measures.Maximum - $measures.Minimum + 1

            # The number of cards being evaluated, the number of unique cards from that set, 
            # and the range must be equal for it to be a run
            if ($range -eq $valueSet.Length -and $range -eq $cards.Length) {
                $points = $cards.Length

                $run = $cards | Sort-Object -Property value # Arrange run in order
                $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]}) # Extract only values, not suit from string

                $message = "Earned $points points for run of [$($ranksInRun -join ", ")]"
                return $points, $message
            }
        }

        # Remove first element from cards being evaluated
        if ($cards.Length -gt 1) {
            $cards = $cards[1..($cards.Length-1)]
        } else {
            $cards = @()
        }
    }
    return 0, ""
}

<#
Checks for any run in the player's hand and scores 
them appropriately. This function applies to phase 3, or
the "show" phase
#>
function scoreRunsPhase3($hand) {
    # Sort hand by value
    $sorted = $hand | Sort-Object -Property value
    
    # Initialize variables
    $score = 0
    $run = @()
    $duplicates = @()
    $message = @()

    # Iterate over each card in hand, checking for runs
    foreach ($card in $sorted) {
        # If run is empty, add card to it 
        if ($run.Length -eq 0) {
            $run += $card
            continue
        }

        # If the current card has the same value as the last card in the run, add it to duplicates
        if ($card.value -eq $run[-1].value) {
            $duplicates += $card
            $duplicates += $run[-1]     # Add last card to duplicates as well

            # This is here in case there a card has been added to duplicates multiples times
            $duplicates = $duplicates | Select-Object -Unique   

            continue # Go back to top of loop 
        }

        # If the current card is the next in the run, add it to the run
        if ($card.value -eq $run[-1].value + 1) {
            $run += $card
        } else {    # If not, check to see if any runs were discovered in the working set, and reset variables
            # If the current run already had 3 or more cards, it was a valid run
            if ($run.Length -ge 3) {
                # Multiply the run length by the number of duplicates found (or one) to get the points for that run
                $points = $run.Length * [Math]::max(1, $duplicates.Length)
                $score += $points   # Update score

                # Add this run to the message
                $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]})  # Extract only values, not suit from string
                $duplicatesString = If ($duplicates.Length -gt 0) {"[$($duplicates -join ", ")] as duplicates"} Else {"no duplicates"}
                $message += "Scored $points points for run of [$($ranksInRun  -join ", ")] with $duplicatesString"
            }

            # Reset variables
            $run = @()
            $duplicates = @()
            $run += $card
        }
    }

    # If the current run has 3 or more cards, it is a valid run
    if ($run.Length -ge 3) {
        # Multiply the run length by the number of duplicates found (or one) to get the points for that run
        $points = $run.Length * [Math]::max(1, $duplicates.Length)
        $score += $points   # Update score

        # Add this run to the message
        $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]})  # Extract only values, not suit from string
        $duplicatesString = If ($duplicates.Length -gt 0) {"[$($duplicates -join ", ")] as duplicates"} Else {"no duplicates"}
        $message += "Scored $points points for run of [$($ranksInRun  -join ", ")] with $duplicatesString"
    }

    return $score, $message
}

function scorePairsPhase3($hand) {
    $score = 0
    $messages = @()

    for ($i = 0; $i -lt $hand.Length; $i++) {
        for ($j = $i + 1; $j -lt $hand.Length; $j++) {
            if ($hand[$i].value -eq $hand[$j].value) {
                $score += 2
                $messages += "Pair found with $($hand[$i]) and $($hand[$j])"
            } else {
                break
            }
        }
    }

    return $score, $messages
}

<#
Builds a standard deck of cards
#>
function buildDeck() {
    $deck = [card[]]::new(52)                           # Allocate space for 52 cards
    $suits = "Spades", "Clubs", "Hearts", "Diamonds"    # Define suits

    $idx=0
    foreach ($suit in $suits) {                         # Iterate through suits
        for ($i = 1; $i -le 13; $i++) {                 # Add values 1-13 (Ace, 2-10, Jack, Queen, King) for each suit
            $card = New-Object Card $i, $suit           # Create card object
            $deck[$idx] = $card                         # Add card to deck
            $idx++
        }
    }
    return $deck
}

function dealHands($deck) {
    # Make sure deck is 52 cards long
    if ($deck.Length -ne 52) {
        throw "Deck size != 52"
    }

    # Get 13 random cards from deck
    $shuffled = $deck | Get-Random -Count 13

    # First six will be one starting hand
    $hand1 = [System.Collections.ArrayList] $shuffled[0..5] | Sort-Object -Property value

    # Second six will be the other starting hand
    $hand2 = [System.Collections.ArrayList] $shuffled[6..11] | Sort-Object -Property value
    
    # Last card is the community card
    $communityCard = $shuffled[12]

    return $hand1, $hand2, $communityCard
}

$DECK = buildDeck
$p1Hand, $p2Hand, $commCard = dealHands($deck)

# Write-Host "Cards:"
# Write-Host $($p1Hand[0..3] + $commCard -join ", ")

# Write-Host "Phase 2:"
# Write-Host (scoreRunsPhase2($p1Hand[0..3] + $commCard))[1]
# Write-Host
# Write-Host "Phase 3:"
# Write-Host ((scoreRunsPhase3($p1Hand[0..3] + $commCard))[1] -join ", ")

function selectCardFromHand($hand) {
    while($true) {
        [int] $index = Read-Host "Select a card index"
        $index--

        if ($index -lt 0 -or $index -ge $hand.Count) {
            Write-Host "Please enter valid input"
            continue
        }

        return $hand[$index]
    }
}

Write-Output ($p1Hand -join ", ")
$selected = selectCardFromHand($p1Hand)
Write-Output "Selected: $selected"