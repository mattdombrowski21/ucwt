<#
Group Members - Capt David Moore, Lt Tristan Bueler, Lt Matt Dombrowski, Lt Matt Underwood
Powershell script for Cyber Incidents Training Mission
#>

<#
Read in file contents from incidents in 
./Incidents subdirectory. Return Array
of hash tables based on record contents for
each incident
#>
function GetIncidentRecords{
    $ret = [System.Collections.ArrayList] @()
    $fileList = Get-ChildItem "./Incidents"

    # Cycle throught each file
    foreach($file in $fileList) { 
        $recordMap = @{"Incident Name"=$file.Name} # Initialize recordMap for this record
        $fileContents = Get-Content $("./Incidents/" + $file.Name) # Read file contents

        # Cycle through each line in the file
        foreach ($line in $fileContents) {
            $splitLine = $line -Split ":" # Split on the ":" to get the key and value separate
            if ($splitLine.Length -eq 2) {
                $recordMap[$($splitLine[0].Trim())] = $splitLine[1].Trim() # Add key/value pair to recordMap
            }
        }
        $ret.Add($recordMap) | Out-Null # Add record to list 
    }

    return $ret
}

<#
Parses the file specified for IP addrs and returns
the IPs in an array
#>
function GetIPs ($file) {
    $ipRegex = "(\b25[0-5]|\b2[0-4][0-9]|\b[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}" # Regular Expression for IP Addrs
    $fileContent = Get-Content $file
    return $fileContent -match $ipRegex
}

<#
Parses the date time string and returns only the year
#>
function GetYearOfAttack($dateTimeString) {
    return [int] $dateTimeString.Substring(10) # Year starts at index 10 in DateTimeString
}

# Init counter variables/maps
$totalSuccessfulAttacks = 0                                             # Total number of successful attacks
$totalOngoingAttacks = 0                                                # Total number of ongoing attack
$stateAttackVectorCounts = @{"China"=@{};"Iran"=@{};"North Korea"=@{}}  # Maps state names to a nested map of attack types to counters
$stateSuccessCounts = @{"China"=0;"Iran"=0;"North Korea"=0}             # Maps state names to successful attack counts
$attackVectorSuccessCounts = @{}                                        # Maps attack types to successful attack counts
$systemTypeSuccessCounts = @{}                                          # Maps system types to successful attacks against that system

# Create map of IP addresses to their country of origin
$ipMap = @{}
$chinaIPs = GetIPs("./chinaIPs.txt")
$iranIPs = GetIPs("./iranIPs.txt")
$nkIPs = GetIPs("./northKoreaIPs.txt")

foreach($ip in $chinaIPs) {
    $ipMap[$ip] = "China"
}
foreach($ip in $iranIPs) {
    $ipMap[$ip] = "Iran"
}
foreach($ip in $nkIPs) {
    $ipMap[$ip] = "North Korea"
}

# Read in file contents
$records = GetIncidentRecords
$ongoingAttackArray = [System.Collections.ArrayList] @()

# Parse each record
foreach($record in $records) {
    # Retrieve the relevant values from the record
    $nationState = $ipMap[$($record["ADDRESS OF SOURCE"].Trim())]
    $attackType = $record["TYPE OF ATTACK"].Trim()
    $numAffected =[int] $record["NUMBER OF SYSTEMS AFFECTED"].Trim()
    $systemAffected = $record["SOFTWARE/SYSTEM AFFECTED"].Trim()
    $year = GetYearOfAttack $record["DTG ATTACK ENDED"].Trim()

    # Ongoing attacks are set to year 3000
    if ($year -eq 3000) {
        $totalOngoingAttacks++
        $record["Nation State"] = $nationState
        $ongoingAttackArray.Add($record) | Out-Null
    }

    $stateAttackVectorCounts[$nationState][$attackType]++
    
    # Ignore the rest if not a successful attack
    if ($numAffected -eq 0) {
        continue
    }

    $totalSuccessfulAttacks++
    $stateSuccessCounts[$nationState]++
    $attackVectorSuccessCounts[$attackType]++
    $systemTypeSuccessCounts[$systemAffected]++
}

# Init result map (used "[ordered]" to preserve order in output)
$result = [ordered]@{}

# Calculate percentage of successful attacks
$frequencyOfSuccess = ([double] $totalSuccessfulAttacks / [double]$records.Length) * 100
$frequencyOfSuccess = [System.Math]::Round($frequencyOfSuccess, 2)
$frequencyOfSuccess = "$($frequencyOfSuccess)%"
$result["Percentage of Successful Attacks"] = "$frequencyOfSuccess ($totalSuccessfulAttacks/$($records.Length))"

<#
Iterate through nested map of states to attack vector types to 
counts and get the most frequent attack vector type for each 
nation state
#>
foreach($stateEnum in $stateAttackVectorCounts.GetEnumerator()) {    
    $mostFrequent = ""
    $maxCount = 0
    foreach($attackEnum in $($stateEnum.Value).GetEnumerator()) {
        $count = $attackEnum.Value

        # If new max is found, update control vars
        if ($count -gt $maxCount) {
            $maxCount = $count
            $mostFrequent = $attackEnum.Name
        }
    }

    $result["$($stateEnum.Name) Most Frequent Attack Vector"] = $mostFrequent
}

# Iterate through state success counts and add to result
foreach($stateEnum in $stateSuccessCounts.GetEnumerator()) {
    $result["$($stateEnum.Name) Success Count"] = $stateEnum.Value
}

# Iterate through attack vector type success counts and add to result
foreach($attackEnum in $attackVectorSuccessCounts.GetEnumerator()) {
    $result["$($attackEnum.Name) Success Count"] = $attackEnum.Value
}

# Iterate through system affected success counts and add to result
foreach($systemEnum in $systemTypeSuccessCounts.GetEnumerator()) {
    $result["$($systemEnum.Name) Times Successfully Attacked"] = $systemEnum.Value
}

$result["Ongoing Attacks"] = $totalOngoingAttacks # Add ongoing attacks to result

# Display Summary Results in Table
$result | Out-GridView -Title "Summary of Cyber Incidents"

# Display detailed info about ongoing attacks in table
$ongoingAttackArray | Select-Object @{Name="Name";Expression={$_["Incident Name"]}},
                    @{Name="Source Nation";Expression={$_["Nation State"]}},
                    @{Name="Attack Type";Expression={$_["TYPE OF ATTACK"]}},
                    @{Name="Unit";Expression={$_["UNIT"]}},
                    @{Name="Date Detected";Expression={$_["DTG ATTACK DETECTED"]}},
                    @{Name="Systems Affected";Expression={$_["SOFTWARE/SYSTEM AFFECTED"]}},
                    @{Name="Number of Systems Affected";Expression={$_["NUMBER OF SYSTEMS AFFECTED"]}} | Out-GridView -Title "Ongoing Attacks"
