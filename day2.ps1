# Exercise 1
cls
$x = Read-Host "Enter a number"

if (!($x % 3)) {
    "$x is divisible by 3"
} else {
    "$x is not divisible by 3"
}

# Exercise 2
$greater = Read-Host "Enter a number"
$extra = Read-Host "Enter another number"
if ($extra -gt $greater){
    $greater = $extra
}
Write-Output "$greater is the greater number"

# Exercise 3
[int] $age = Read-Host "Enter your age"
switch ($age) {
    {$_ -lt 13} {Write-Output "Younger than 13"}
    {$_ -ge 13 -and $_ -lt 20} {Write-Output "Teenager"}
    {$_ -ge 20 -and $_ -lt 30} {Write-Output "20s"}
default {Write-Output "Older than 30"}
}

# Exercise 4
[int] $grade = Read-Host "Enter your grade"
switch ($grade) {
    {$_ -lt 0 -or $_ -gt 100} {Write-Output "Invalid"}
    {$_ -ge 90} {Write-Output "A"; break}
    {$_ -ge 80} {Write-Output "B"; break}
    {$_ -ge 70} {Write-Output "C"; break}
    {$_ -ge 60} {Write-Output "D"; break}
default {Write-Output "F"}
}