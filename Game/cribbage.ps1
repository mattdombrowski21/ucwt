<#
Class for cards. Cards are represented by value and suit. 
Value is represented as an int from 1-13 and converted to 
it's string representation in the "ToString()" function
#>
class Card {
    [int]$value
    [string]$suit

    static $valueToStringMap = @{1="Ace";2="Two";3="Three";4="Four";5="Five";
                                6="Six"; 7="Seven"; 8="Eight";9="Nine"; 10="Ten"; 
                                11="Jack"; 12="Queen";13="King"}

    Card([int]$value, [string]$suit) {
        $this.value = $value
        $this.suit = $suit
    }

    [string] ToString() {
        return "$([Card]::valueToStringMap[$this.value]) of $($this.suit)"
    }
}

<#
Ring buffer for printing most recent messages to the UI.
Older messages are pushed out when the buffer is full.
Method defined for adding elements (Add) and getting an 
array representation of the current contents (GetContents)
#>
class RingBuffer {
    [string[]]$buffer   # Actual buffer holding contents (fixed size)
    [int]$currIdx       # Next index to add an element to
    [int]$size          # Size of the buffer
    [bool]$full         # Measures if the buffer is full or not
    [int]$oldest        # Holds the index (in the buffer array) of the oldest element
    
    # Constructor
    RingBuffer([int] $size) {
        $this.buffer = [string[]]::new($size)
        $this.currIdx = 0
        $this.size = $size
        $this.full = $false
        $this.oldest = -1
    }

    # Adds an element to the ring buffer. If it is full, it starts pushing old elements out
    [void] Add($element) {
        # Add new element to buffer
        $this.buffer[$this.currIdx] = $element
        $this.currIdx++

        if ($this.oldest -eq -1) {
            $this.oldest = 0
        }

        # If the buffer is already full, an element must be pushed out. Move oldest index
        if ($this.full){
            $this.oldest++
            if ($this.oldest -eq $this.size) {
                $this.oldest = 0
            }
        }

        # If the next index is out of bounds, reset it to 0 and mark the buffer as full
        if ($this.currIdx -eq $this.size) {
            $this.currIdx = 0
            $this.full = $true
        }
    }

    # Returns the contents of the ring buffer in an array in the order they were inserted
    [string[]] GetContents() {
        $ret = @()

        # If oldest is -1, the buffer is empty
        if ($this.oldest -eq -1) {
            return $ret 
        }

        # Add elements to return array starting from the oldest element to the end of the buffer
        for ($i = $this.oldest; $i -lt $this.size; $i++) {
            $ret += $this.buffer[$i]
        }

        # Need to account for elements in underlying buffer that come between 0 and oldest idx
        for ($i = 0; $i -lt $this.oldest; $i++) {
            $ret += $this.buffer[$i]
        }
        
        return $ret
    }
}

<#
Builds a standard deck of cards
#>
function buildDeck() {
    $deck = [card[]]::new(52)                           # Allocate space for 52 cards
    $suits = "Spades", "Clubs", "Hearts", "Diamonds"    # Define suits

    $idx=0
    foreach ($suit in $suits) {                         # Iterate through suits
        for ($i = 1; $i -le 13; $i++) {                 # Add values 1-13 (Ace, 2-10, Jack, Queen, King) for each suit
            $card = New-Object Card $i, $suit           # Create card object
            $deck[$idx] = $card                         # Add card to deck
            $idx++
        }
    }
    return $deck
}

function dealHands($deck) {
    # Make sure deck is 52 cards long
    if ($deck.Length -ne 52) {
        throw "Deck size != 52"
    }

    # This is just for fun. It simulates it taking time to deal cards
    Clear-Host
    # Write-Host "`n`n`n`n`n"
    # [Console]::CursorVisible = $false
    # for ($i = 0; $i -lt 5; $i++) {
    #     Write-Host "`r`t`tDealing cards" -NoNewLine
    #     Start-Sleep -Milliseconds 300
    #     Write-Host "`r`t`tDealing cards .        " -NoNewLine
    #     Start-Sleep -Milliseconds 300
    #     Write-Host "`r`t`tDealing cards . .      " -NoNewLine
    #     Start-Sleep -Milliseconds 300
    #     Write-Host "`r`t`tDealing cards . . .    " -NoNewLine
    #     Start-Sleep -Milliseconds 300
    #     Write-Host "`r`t`tDealing cards . . . .  " -NoNewLine
    #     Start-Sleep -Milliseconds 300
    #     Write-Host "`r`t`tDealing cards . . . . ." -NoNewLine
    # }
    # [Console]::CursorVisible = $true
    # Clear-Host

    # Get 13 random cards from deck
    $shuffled = $deck | Get-Random -Count 13

    # First six will be one starting hand
    [System.Collections.ArrayList] $hand1 = $shuffled[0..5] | Sort-Object -Property value

    # Second six will be the other starting hand
    [System.Collections.ArrayList] $hand2 = $shuffled[6..11] | Sort-Object -Property value
    
    # Last card is the community card
    $communityCard = $shuffled[12]

    return $hand1, $hand2, $communityCard
}

<#
Print the intro title
#>
function PrintTitle {
    Clear-Host
    $firstLine = ".------..------..------..------..------..------..------..------." 
    $startingPoint = [Math]::Max(0, $Host.UI.RawUI.BufferSize.Width / 3) - [Math]::Floor($firstLine.Length / 2)
    if ($startingPoint -lt 1) {
        $startingPoint = 1
    }
    $space = ' ' * $startingPoint
    Write-Host
    Write-Host
    Write-Host
    Write-Host ("{0}{1}" -f $space, $firstLine)
    Write-Host ("{0}{1}" -f $space, "|C.--. ||R.--. ||I.--. ||B.--. ||B.--. ||A.--. ||G.--. ||E.--. |")
    Write-Host ("{0}{1}" -f $space, "| :/\: || :(): || (\/) || :(): || :(): || (\/) || :/\: || (\/) |")
    Write-Host ("{0}{1}" -f $space, "| :\/: || ()() || :\/: || ()() || ()() || :\/: || :\/: || :\/: |")
    Write-Host ("{0}{1}" -f $space, "| '--'C|| '--'R|| '--'I|| '--'B|| '--'B|| '--'A|| '--'G|| '--'E|")
    Write-Host ("{0}{1}" -f $space, "``------`'``------`'``------`'``------`'``------`'``------`'``------`'``------`'")
    Write-Host
    Write-Host
    Write-Host
    Write-Host "`tPress ENTER to continue"
    $null = Read-Host
}

<#
Draws the UI
Shows current player, P1 and P2 scores, the currently played cards,
the player's hand (including community card), and a log of messages
(i.e. breakdown of how points were earned)
#>
function drawUI($hand, $log, $communityCard, $playedCards) {
    Clear-Host  # clear the screen to redraw UI
    Write-Host 
    Write-Host 
    Write-Host 

    # Write current player at top
    $title = "Player $script:currentPlayer"
    $centerPoint = [Math]::Max(0, $Host.UI.RawUI.BufferSize.Width / 3) - [Math]::Floor($title.Length / 2)
    $dividerStart = [Math]::Floor($centerPoint / 2)
    $dividerLen = $dividerStart * 2 + $title.Length + 1
    Write-Host ("{0}{1}" -f (' ' * $centerPoint), $title)
    Write-Host ("{0,-$dividerStart}{1}" -f (' ', ('-' * $dividerLen)))
    Write-Host 

    # Write P1 Score and P2 Score
    $spacing = $dividerLen - "P2 Score: $p2Score".Length
    Write-Host ("{0}{1,-$spacing}{2}" -f (' ' * $dividerStart), "P1 Score: $p1Score ", "P2 Score: $p2Score")
    Write-Host
    Write-Host
    Write-Host

    # If cards have been played, print them
    if ($playedCards.Count -gt 0) {
        Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Played Cards".Length + 4)))
        Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), "| Played Cards |")
        Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Played Cards".Length + 4)))
        Write-Host 

        # Print each card that has been played
        $idx = 1
        foreach($card in $playedCards) {
            Write-Host ("{0}{1,-23}" -f (' ' * $($centerPoint - 3)), "$idx.) $card")
            $idx++
        }
        Write-Host
        Write-Host
    }

    # Print player's hand
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Current Hand".Length + 4)))
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), "| Current Hand |")
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Current Hand".Length + 4)))
    Write-Host 
    if ($communityCard.value -gt 0) {
        Write-Host ("{0}{1}" -f (' ' * $centerPoint), "$communityCard (Community Card)")    # Show community card
        Write-Host 
    }
    
    if ($Host.UI.RawUI.BufferSize.Width -le 80) {
        $idx = 1
        foreach($card in $hand) {
            Write-Host ("{0}$idx. {1}" -f (' ' * $centerPoint), $card)
            Write-Host
            $idx++
        }
    } else {
        $idx = 1
        foreach($card in $hand) {
            $spacing = $centerPoint - 4
            Write-Host ("    $idx. {0, -$spacing}" -f $card) -NoNewline
            if ($idx % 3 -eq 0) {
                Write-Host
                Write-Host
            }
            $idx++
        } 
    }
    Write-Host
    Write-Host

    # Print log messages
    $messages = $log.GetContents()
    if ($messages.Length -eq 0) {return}
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Log".Length + 4)))
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), "| Log |")
    Write-Host ("{0}{1}" -f (' ' * $($centerPoint - 2)), ('-' * $("Log".Length + 4)))
    Write-Host 
    foreach ($message in $messages) {
        Write-Host ("{0}{1}" -f (' ' * $dividerStart), $message)
    }
}

<#
Scores the players play and increments their score
#>
function scorePhase2($cardsPlayed, $hand, $playerNum) {
    $playerName = If ($playerNum -eq 1) {"Player 1"} Else {"Player 2"}

    $pairsScore, $messages = scorePairsPhase2 $cardsPlayed
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $pairsScore} else { $script:p2Score += $pairsScore}
    drawUI $hand $script:log $commCard $cardsPlayed
    Start-Sleep -Milliseconds 200

    $fifteensScore, $messages = scoreFifteensPhase2 $cardsPlayed
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $fifteensScore} else { $script:p2Score += $fifteensScore}
    drawUI $hand $script:log $commCard $cardsPlayed
    Start-Sleep -Milliseconds 200

    $runsScore, $messages = scoreRunsPhase2 $cardsPlayed
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $runsScore} else { $script:p2Score += $runsScore}
    drawUI $hand $script:log $commCard $cardsPlayed
    Start-Sleep -Milliseconds 200
}

<#
Checks if the last card played creates any pairs and scores 
it appropriately
#>
function scorePairsPhase2($cards) {
    $score = 0
    $messages = @()
    
    for ($i = $cards.length - 1; $i -ge 0; $i--) {
        for ($j = $i - 1; $j -ge 0; $j--) {
            if ($cards[$i].value -eq $cards[$j].value) {
             $score += 2
             $messages += "Scored 2 points for pair with $($cards[$i]) and $($cards[$j])"
            } else {
                $i = 0
                break
            }
        }
    }
    return $score, $messages
}

<#
Checks if the last card played creates a sum of 
15 with the previous cards and scores it appropriately
#>
function scoreFifteensPhase2($cards) {
    $sum = 0
    $cardsInSum = @()
    for ($i = $cards.Length - 1; $i -ge 0; $i--) {
        $sum += [Math]::min($cards[$i].value, 10)
        $cardsInSum += $cards[$i]
        switch ($sum) {
            {$_ -eq 15} {
                $message = "Scored 2 points for [$($cardsInSum -join ", ")] summing 15"
                return 2, $message
            }
            {$_ -gt 15} {
                return 0, ""
            }
            {$_ -lt 15} {
                continue
            }
        }
    }
    return 0, ""
}

<#
Checks for runs in the currently played cards and scores 
them appropriately. This function applies to phase 2, or
the "pegging" phase
#>
function scoreRunsPhase2($cards) {
    while ($cards.Length -gt 0) {
        # Get unique values from cards being evaluated
        $valueSet = $cards | Select-Object -Property value -Unique

        # Measure the count, max, and min values in the set
        $measures = $valueSet | Measure-Object -Property value -Maximum -Minimum
        
        # If there is potential for a run (set length > 2), continue checking 
        if ($measures.Count -gt 2) {
            # Range will be the difference between the highest and lowest value cards
            $range = $measures.Maximum - $measures.Minimum + 1

            # The number of cards being evaluated, the number of unique cards from that set, 
            # and the range must be equal for it to be a run
            if ($range -eq $valueSet.Length -and $range -eq $cards.Length) {
                $points = $cards.Length

                $run = $cards | Sort-Object -Property value # Arrange run in order
                $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]}) # Extract only values, not suit from string

                $message = "Earned $points points for run of [$($ranksInRun -join ", ")]"
                return $points, $message
            }
        }

        # Remove first element from cards being evaluated
        if ($cards.Length -gt 1) {
            $cards = $cards[1..($cards.Length-1)]
        } else {
            $cards = @()
        }
    }
    return 0, ""
}

<#
Scores the players hand and increments their score
#>
function scorePhase3($hand, $commCard, $playerNum) {
    $playerName = If ($playerNum -eq 1) {"Player 1"} Else {"Player 2"}

    [System.Collections.ArrayList] $fullHand = $hand.Clone()
    $fullHand.Add($commCard) | Out-Null

    $pairsScore, $messages = scorePairsPhase3 $fullHand
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $pairsScore} else { $script:p2Score += $pairsScore}
    drawUI $hand $script:log $commCard
    Start-Sleep -Milliseconds 200

    $fifteensScore, $messages = scoreFifteensPhase3 $fullHand
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $fifteensScore} else { $script:p2Score += $fifteensScore}
    drawUI $hand $script:log $commCard
    Start-Sleep -Milliseconds 200

    $runsScore, $messages = scoreRunsPhase3 $fullHand
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $runsScore} else { $script:p2Score += $runsScore}
    drawUI $hand $script:log $commCard
    Start-Sleep -Milliseconds 200

    $flushesScore, $messages = scoreFlushes $hand $commCard
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $flushesScore} else { $script:p2Score += $flushesScore}
    drawUI $hand $script:log $commCard
    Start-Sleep -Milliseconds 200

    $jackScore, $messages = scoreTheRightJack $hand $commCard
    foreach ($message in $messages) {
        if ($message -ne "") {$script:log.Add("$playerName $message")}
    }
    if ($playerNum -eq 1) { $script:p1Score += $jackScore} else { $script:p2Score += $jackScore}
    drawUI $hand $script:log $commCard
}

<#
Scores all possible combinations of pairs in a players hand.
This is used during phase 3 only
#>
function scorePairsPhase3($cards) {
    $sorted = $cards | Sort-Object -Property value
    $score = 0
    $messages = @()
    for ($i = 0; $i -lt $sorted.length; $i++) {
        for ($j = $i + 1; $j -lt $sorted.length; $j++) {
            if ($sorted[$i].value -eq $sorted[$j].value) {
             $score += 2
             $messages += "Scored 2 points for pair with $($sorted[$i]) and $($sorted[$j])"
            } else {
                break
            }
        }
    }
    return $score, $messages
}

$script:combinations = [System.Collections.ArrayList] @()   # Used in recursive combination function
$script:combination = [System.Collections.ArrayList] @()    # Used in recursive combination function

<#
Recursive function to accumulate all combinations of cards into the 
"combinations" variable. This should not be called directly, only use 
the "getCombinations" function
#>
function getCombinationsRecursion($cards, $start) {
    for ($i = $start; $i -lt $cards.Count; $i++) {
        $script:combination.Add($cards[$i]) | Out-Null
        $script:combinations.Add($script:combination.Clone()) | Out-Null
        if ($i -lt $cards.Count) {
            getCombinationsRecursion $cards ($i + 1)
        }
        if ($script:combination.Count -gt 0) {
            $script:combination.RemoveAt($script:combination.Count - 1)
        }
    }
}

<#
Returns all possible combinations of the cards passed in.
Calls the "getCombinationsRecursion" function after resetting
control variables
#>
function getCombinations($cards) {
    $script:combinations = [System.Collections.ArrayList] @()
    $script:combination = [System.Collections.ArrayList] @()
    getCombinationsRecursion $cards 0
    $ret = $combinations.Clone()
    $script:combinations = [System.Collections.ArrayList] @()
    $script:combination = [System.Collections.ArrayList] @()
    return $ret
}

<#
Scores all of the combinations of cards that add up to 
15. This is used during phase 3 only 
#>
function scoreFifteensPhase3($cards) {
    $score = 0
    $messages = @()
    $combos = getCombinations $cards
    foreach($combo in $combos) {
        $sum = 0
        foreach($card in $combo) {
            $sum += [Math]::min($card.value, 10)
        }
        if ($sum -eq 15) {
            $score += 2
            $messages += "Scored 2 points for [$($combo -join ", ")] summing 15"
        }
    }

    return $score, $messages
}

<#
Checks for any run in the player's hand and scores 
them appropriately. This function applies to phase 3, or
the "show" phase
#>
function scoreRunsPhase3($hand) {
    # Sort hand by value
    $sorted = $hand | Sort-Object -Property value
    
    # Initialize variables
    $score = 0
    $run = @()
    $duplicates = @()
    $message = @()

    # Iterate over each card in hand, checking for runs
    foreach ($card in $sorted) {
        # If run is empty, add card to it 
        if ($run.Length -eq 0) {
            $run += $card
            continue
        }

        # If the current card has the same value as the last card in the run, add it to duplicates
        if ($card.value -eq $run[-1].value) {
            $duplicates += $card
            $duplicates += $run[-1]     # Add last card to duplicates as well

            # This is here in case there a card has been added to duplicates multiples times
            $duplicates = $duplicates | Select-Object -Unique   

            continue # Go back to top of loop 
        }

        # If the current card is the next in the run, add it to the run
        if ($card.value -eq $run[-1].value + 1) {
            $run += $card
        } else {    # If not, check to see if any runs were discovered in the working set, and reset variables
            # If the current run already had 3 or more cards, it was a valid run
            if ($run.Length -ge 3) {
                # Multiply the run length by the number of duplicates found (or one) to get the points for that run
                $points = $run.Length * [Math]::max(1, $duplicates.Length)
                $score += $points   # Update score

                # Add this run to the message
                $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]})  # Extract only values, not suit from string
                $duplicatesString = If ($duplicates.Length -gt 0) {"[$($duplicates -join ", ")] as duplicates"} Else {"no duplicates"}
                $message += "Scored $points points for run of [$($ranksInRun  -join ", ")] with $duplicatesString"
            }

            # Reset variables
            $run = @()
            $duplicates = @()
            $run += $card
        }
    }

    # If the current run has 3 or more cards, it is a valid run
    if ($run.Length -ge 3) {
        # Multiply the run length by the number of duplicates found (or one) to get the points for that run
        $points = $run.Length * [Math]::max(1, $duplicates.Length)
        $score += $points   # Update score

        # Add this run to the message
        $ranksInRun = $run.ForEach({($_.ToString() -split " of ")[0]})  # Extract only values, not suit from string
        $duplicatesString = If ($duplicates.Length -gt 0) {"[$($duplicates -join ", ")] as duplicates"} Else {"no duplicates"}
        $message += "Scored $points points for run of [$($ranksInRun  -join ", ")] with $duplicatesString"
    }

    return $score, $message
}

function scoreFlushes($cards, $commCard) {
    $foundSuits = @{}
    foreach ($card in $cards){
        $foundSuits[$card.suit]++
    }

    foreach ($suitEnum in $foundSuits.GetEnumerator()) {
        $suit = $suitEnum.Name
        $count = $suitEnum.Value
        if ($count -ge 4) {
            if ($commCard.suit -eq $suit) {
                return 5, "Earned 5 points for a flush of $suit"
            }
            return 4, "Earned 4 points for a flush of $suit"
        }
    }
    
    return 0, ""
}

<#
Checks if the hand has the jack of the same suit as the community card
and scores it appropriately
#>
function scoreTheRightJack($cards, $commCard) {
    foreach ($card in $cards) {
        if ($card.value -eq 11 -and $card.suit -eq $commCard.suit) {
            return 1, "Scored 1 for having the right Jack"
        }
    }
}

<#
Switches control to the opposite player
#>
function switchPlayer {
    $script:currentPlayer = $script:currentPlayer % 2 + 1
    Clear-Host  # clear the screen to redraw UI
    Write-Host "`n`n`n`n`n`n`n`n"
    Write-Host "`t`t`tPlayer $script:currentPlayer, press ENTER to continue"
    Write-Host "`n`n`n`n`n`n`n`n"
    $null = Read-Host
}

function selectCardFromHand($hand) {
    while($true) {
        [int] $index = Read-Host "Select a card index"
        $index--

        if ($index -lt 0 -or $index -ge $hand.Count) {
            Write-Host "Please enter valid input"
            continue
        }

        return $hand[$index]
    }
}

function phase1 {
    # Deal 13 random cards
    $p1hand, $p2hand, $commCard = dealHands $script:DECK 

    $hands = @($p1hand, $p2hand)
    $crib = [System.Collections.ArrayList] @()    

    for ($i = 0; $i -le 1; $i++) {
        $hand = $hands[$script:currentPlayer - 1]
        drawUI $hand $script:log $commCard
        Write-Host "Select a card to DISCARD"
        $discard = selectCardFromHand $hand
        $hand.Remove($discard) | Out-Null
        $crib.Add($discard) | Out-Null

        drawUI $hand $script:log $commCard
        Write-Host "Select a card to DISCARD"
        $discard = selectCardFromHand $hand
        $hand.Remove($discard) | Out-Null
        $crib.Add($discard) | Out-Null

        switchPlayer
    }
    [System.Collections.ArrayList] $p1Hand = $hands[0]
    [System.Collections.ArrayList] $p2Hand = $hands[1]

    return $p1Hand, $p2Hand, $crib, $commCard
}

function hasPlayableCard($hand, $currCount) {
    [int] $minCardValue = ($hand | Measure-Object -Property value -Minimum).Minimum
    [int] $minCardValue = [Math]::min($minCardValue, 10)
    return ($hand.Count -gt 0 -and $minCardValue + $currCount -le 31)
}

function phase2($p1Hand, $p2Hand, $commCard) {
    $count = 0
    $cardsplayed = @()
    $lastPlayed = 0

    while ($p1Hand.Count -gt 0 -or $p2Hand.Count -gt 0) {
        [System.Collections.ArrayList] $hand = If ($script:currentPlayer -eq 1) {$p1Hand} Else {$p2Hand}
        
        $player1HasPlayable = hasPlayableCard $p1Hand $count
        $player2HasPlayable = hasPlayableCard $p2Hand $count

        if (!$player1HasPlayable -and !$player2HasPlayable) {
            if ($lastplayed -eq 1) {
                $script:p1Score += 1
                $script:log.Add("Player 1 Scored 1 point for a Go")
            } else {
                $script:p2Score += 1
                $script:log.Add("Player 2 Scored 1 point for a Go")
            }
            drawUI $hand $script:log $commCard $cardsPlayed
            Write-Host "No one has any cards to play. Resetting the count...`n`nPress ENTER to continue"
            $null = Read-Host
            checkWinner

            $count = 0
            $cardsplayed = @()
            continue
        }
        
        if ($script:currentPlayer -eq 1) {
            if (!$player1hasPlayable) {
                Write-Host "You do not have a card to play. Press ENTER to continue"
                $null = Read-Host
                switchPlayer
                continue
            }
        } else {
            if (!$player2hasPlayable) {
                switchPlayer
                Write-Host "You do not have a card to play. Press ENTER to continue"
                $null = Read-Host
                continue
            }
        }
        drawUI $hand $script:log $commCard $cardsPlayed
        
        Write-Host "Current count is $count"
        Write-Host "Select a card to PLAY"

        $cardPlayed = $null
        while ($null -eq $cardPlayed) {
            $cardPlayed = selectCardFromHand $hand
            if ([Math]::min($cardPlayed.value, 10) + $count -le 31) {
                break
            } else {
                Write-Host "$cardPlayed brings the value above 31. Select another card."
            }
        }
        $hand.Remove($cardPlayed)
        if ($script:currentPlayer -eq 1) {$p1Hand = $hand} else {$p2Hand = $hand}
        drawUI $hand $script:log $commCard $cardsPlayed

        $count += [Math]::min($cardPlayed.value, 10)
        $cardsPlayed += $cardPlayed

        $lastPlayed = $script:currentPlayer
        scorePhase2 $cardsPlayed $hand $script:currentPlayer
        checkWinner

        if ($count -eq 31) {
            if ($script:currentPlayer -eq 1) {
                $script:p1Score += 2
                $script:log.Add("Player 1 Scored 2 points for getting to 31")
            } else {
                $script:p2Score += 2
                $script:log.Add("Player 2 Scored 2 points for getting to 31")
            }
            drawUI $hand $script:log $commCard $cardsPlayed
            Start-Sleep -Milliseconds 1000
            checkWinner

            $count = 0
            $cardsplayed = @()
        }
        checkWinner
        
        Write-Host "Press ENTER to continue"
        $null = Read-Host
        switchPlayer
    }

    if ($lastplayed -eq 1) {
        $script:p1Score += 1 
        $script:log.Add("Player 1 Scored 1 point for playing the last card")
    } else {
        $script:p2Score += 1
        $script:log.Add("Player 2 Scored 1 point for playing the last card")
    }
    drawUI $hand $script:log $commCard $cardsPlayed
    Start-Sleep -Milliseconds 1000
    checkWinner
}

function phase3 {
    Write-Host "We are now in the show phase. Both players may look at the screen.`nPress ENTER to continue"
    $null = Read-Host
    if ($script:currPlayerGoingFirst -eq 1) {
        Clear-Host
        Write-Host "Scoring Player 1's Hand..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $p1Hand $commCard 1
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host

        Clear-Host
        Write-Host "Scoring Player 2's Hand..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $p2Hand $commCard 2
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host

        Clear-Host
        Write-Host "Scoring the Crib for Player 2..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $crib $commCard 2
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host
    } else {
        Clear-Host
        Write-Host "Scoring Player 2's Hand..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $p2Hand $commCard 2
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host

        Clear-Host
        Write-Host "Scoring Player 1's Hand..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $p1Hand $commCard 1
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host

        Clear-Host
        Write-Host "Scoring the Crib for Player 1..."
        Start-Sleep -Milliseconds 1500
        scorePhase3 $crib $commCard 1
        checkWinner
        Write-Host "Press ENTER to continue"
        $null = Read-Host
    }
}

function checkWinner {
    if ($script:p1Score -ge $script:TARGET_SCORE -or $script:p2Score -ge $script:TARGET_SCORE) {
        $message = If ($script:p1Score -ge $script:TARGET_SCORE) {"Player 1 Wins!"} Else {"Player 2 Wins!"}
        Write-Host $message -NoNewline
        $null = Read-Host
        exit
    }
}

$script:TARGET_SCORE = 121
$script:DECK = buildDeck

$script:currPlayerGoingFirst = 1
$script:currentPlayer = 1
$script:p1Score = 0
$script:p2Score = 0

$script:log = [RingBuffer]::new(10)


printTitle

while ($true) {
    $script:currentPlayer = $script:currPlayerGoingFirst % 2 + 1
    switchPlayer
    
    $p1Hand, $p2Hand, $crib, $commCard = phase1

    $script:currentPlayer = $script:currPlayerGoingFirst
    phase2 $p1Hand.Clone() $p2Hand.Clone() $commCard
    checkWinner

    $script:currentPlayer = $script:currPlayerGoingFirst
    phase3 $p1Hand $p2Hand $crib $commCard
    checkWinner

    $script:currPlayerGoingFirst = $script:currPlayerGoingFirst % 2 + 1
    Clear-Host
    Write-Host "Whoops! No one won yet. Prepare for another round!`n`nPress ENTER to continue"
    $null = Read-Host 
}