# $arrayList = [System.Collections.ArrayList] @()
# Measure-Command -Expression {
#     @(0..20000).foreach({$arrayList.Add($_)})
# }

# $array = @()
# Measure-Command -Expression {
#     @(0..20000).foreach({$array += $_})
# }

# Exercise 1
Write-Output "Exercise 1"
$num = 0
while ($num -le 100) {
    $num
    $num += 5
}

# Exercise 2
Write-Output "`n`nExercise 2"
for ($i = 1; $i -le 10; $i++) {
    $res = If ($i % 2 -eq 0) {"even"} Else {"odd"}
    "$i is $res"
}

# Exercise 3
Write-Output "`n`nExercise 3"
$array = @(1..5)
foreach($num in $array) {
    $res = If ($num % 3 -eq 0) {"is divisible by 3"} Else {""}
    "$num $res"
}

# Exercise 4
Write-Output "`n`nExercise 4 (do/until)"
$num = 0
do {
    $num
    $num += 2
} until ($num -gt 20)

Write-Output "`nExercise 4 (do/while)"
$num = 0
do {
    $num
    $num += 2
} while ($num -le 20)

# Exercise 5
Write-Output "`n`nExercise 5"
$animals = @("Dog", "Otter", "Monkey", "Panda", "Lizard")
foreach($animal in $animals) {
    $animal
}

# Exercise 6
Write-Output "`n`nExercise 6"
$fibSeq = [System.Collections.ArrayList] @(0, 1)
while ($fibSeq[-1] -le 100) {
    $fibSeq.Add($fibSeq[-2] + $fibSeq[-1]) | Out-Null
}
Write-Output $($fibSeq -lt 100)

$fibSeq.BinarySearch(55)