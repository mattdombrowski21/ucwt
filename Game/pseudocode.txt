function drawUI{
    display current player
    display player scores
    display played cards
    display player's hand & commCard
}

function score {
    tally points according to rules
}

function dealHands (deck) {
    shuffled = select 13 random cards from deck
    p1Hand = shuffled[0:5]
    p2Hand = shuffled[6:11]
    commCard = shuffled[12]
    return p1Hand, p2Hand, commCard
}

function phase1 {
    dealHands
    drawUI for player 1
    # Ask what 2 cards p1 wants to discard
    while(validCard not selected) {
        discard1 = GetUserInput
    }
    while(validCard not selected) {
        discard2 = GetUserInput
    }

    drawUI for player 2
    
    # Ask what 2 cards p2 wants to discard
    while(validCard not selected) {
        discard3 = GetUserInput
    }
    while(validCard not selected) {
        discard4 = GetUserInput
    }

    crib = discard1, discard2, discard3, discard4
}

function phase2 {
    cardsPlayed = ArrayList of cards
    totalPointsPlayed = 0
    while (totalPointsPlayed < 31) {
        drawUI for current player
        if (player has playable card) {
            # Ask what cards player wants to play
            while(validCard not selected) {
                card = GetUserInput
            }
            cardsPlayed += card
            score(cardsPlayed)
            totalPointsPlayed += card.value
        } else {
            otherPlayerScore++
        }
        switchPlayers
    }
}

function phase3 {
    playerGoingFirstScore += score(playerGoingFirstHand + commCard)
    otherPlayerScore += score(otherPlayerHand + commCard)
    showOtherPlayer the crib (drawUI)
    otherPlayerScore += score(crib + commCard)
}

TARGET_POINTS = 121
p1Score = 0
p2Score = 0
currPlayerGoingFirst = 1

while (p1Score < TARGET_POINTS and p2Score < TARGET_POINTS) {
    phase1()
    phase2()
    phase3()
    switchCurrPlayerGoingFirst
}