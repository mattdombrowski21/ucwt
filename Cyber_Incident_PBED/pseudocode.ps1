# Variables to track info
$totalSuccessful = 0
$ongoingAttacks = 0
$attackVectorFrequenciesByState = map[State]map[AttackVector]Counter
$stateSuccessFrequencies = map[State]Counter
$attackVectorSuccessFrequencies = map[AttackVector]Counter
$systemSuccessFrequencies = map[System Affected]Counter


# Read in file contents to 2D array
$fileList = gci of "./Incidents"
$fileContents = @() # 2D array
foreach($file in $fileList) {
    $fileContents = cat $file
    $fileContents += $fileContents
}

# Read in IPs & create map of IPs to State
$ipMap = map[IP_Addrs]States
$chinaIPs = cat chinaIPs.txt
$iranIPs = cat iranIPs.txt
$nkIPs = cat northKoreaIPs.txt

foreach($ip in chinaIPs) {
    ipMap[$ip] = "China"
}
foreach($ip in iranIPs) {
    ipMap[$ip] = "Iran"
}
foreach($ip in nkIPs) {
    ipMap[$ip] = "NK"
}

# Parse through earch record and update counter variables
foreach($record in $fileContents) {
    updateCounters $record
}

function retrieveValueFromString ($line) {
    # Split the string by ":"
    # Access 2nd element (value)
    # Trim whitespace from 2nd element
    # Retrun trimmed 2nd element
}

function parseDateTimeString ($dateTimeString) {
    # Extract year from string
    # Return year
}

function updateCounters ($record) {
    # Get IP addr ($ipAddr)
    retrieveValueFromString $lineWithIP
    # Get attack type ($type)
    retrieveValueFromString $lineWithAttackType
    # Get num systems affected ($numAffected)
    retrieveValueFromString $lineWithNumSystemsAffected
    # Get system affected ($sysAffected)
    retrieveValueFromString $lineWithSysAffected
    
    # Get year of attack & parse date/time string
    retrieveValueFromString $lineWithDateAttackEnded
    # Parse date/time string
    $year = retrieveValueFromString $lineWithDateAttackEnded | parseDateTimeString

    $state = $ipMap[$ipAddr]

    if $numAffected > 0 {
        totalSuccessful++
        stateSuccessFrequencies[$state]++
        $attackVectorSuccessFrequencies[$type]++
        $systemSuccessFrequencies[$sysAffected]++
    }

    $attackVectorFrequenciesByState[$state][$type]++

    if $year -eq 3000 {
        $ongoingAttacks++
    }
}

# Printout data all pretty-like
